package cmd

import (
	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "pomi",
	Short: "Paragon Odoo Manufacturing integrator",
	Long:  "Paragon Odoo Manufacturing Integrator",
}

// Execute root function execution
func Execute() {
	rootCmd.Execute()
}
