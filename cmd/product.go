package cmd

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/anezch/pomi/worker"
)

var productCmd = &cobra.Command{
	Use: "product",
}

// WriteSince is a flag variable to determine start of create or write data to be read from NBM
var WriteSince string

var nbm2mfgCmd = &cobra.Command{
	Use:   "nbm2mfg <nbm_server> <mfg_server>",
	Short: "Copy inexist product from NBM server to MFG server",
	Args:  cobra.ExactArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		err := worker.ProductNbm2Mfg(args[0], args[1], WriteSince)
		if err != nil {
			log.Error(err)
		}
	},
}

func init() {
	nbm2mfgCmd.Flags().StringVarP(&WriteSince, "since", "s", "", "Filter creation or update timestamp")
	productCmd.AddCommand(nbm2mfgCmd)

	rootCmd.AddCommand(productCmd)
}
