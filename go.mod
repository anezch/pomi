module gitlab.com/anezch/pomi

go 1.13

require (
	github.com/ausuwardi/godooj v0.1.1
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.6
)
