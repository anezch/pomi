package main

import (
	"gitlab.com/anezch/pomi/cmd"
)

func main() {
	cmd.Execute()
}
