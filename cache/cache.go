package cache

// From: https://medium.com/@melvinodsa/building-a-high-performant-concurrent-cache-in-golang-b6442c20b2ca

// RequestType is uint to determine the type of a request
type RequestType uint

const (
	// READ from cache
	READ RequestType = iota
	// WRITE to cache
	WRITE
	// DELETE from cache
	DELETE
)

// Request facilitates the read, write, and delete to the cache go routine
type Request struct {
	// Request type
	Type RequestType
	// Payload to be written to the cache if a write operation
	Payload interface{}
	// Loader is a function to load data if it doesn't exist
	Loader func(string) interface{}
	// Key is the key of the payload to be read or written to
	Key string
	// Out chan is for getting the response in case of read operation
}

// RequestChannel through which the requests can be made
var RequestChannel = make(chan Request)

func init() {
	go Cache(RequestChannel)
}

// Cache is the in-memory cache go routine
func Cache(ch chan Request) {
	cache := make(map[string]interface{})
	for {

	}
}
