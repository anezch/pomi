package worker

import (
	"strings"
	"sync"

	odoo "github.com/ausuwardi/godooj"
	log "github.com/sirupsen/logrus"
)

type productInfo struct {
	id          int
	name        string
	description string
	code        string
	categID     *odoo.Many2oneFK
	uomID       *odoo.Many2oneFK
}

type uomConversion struct {
	from       string
	to         string
	conversion float64
}

var uomMapping = map[string]*uomConversion{
	"PC(S)":              &uomConversion{"PC(S)", "Pc(s)", 1},
	"KG":                 &uomConversion{"KG", "g", 1000},
	"G":                  &uomConversion{"G", "g", 1},
	"KG (PLASTIK SEGEL)": &uomConversion{"KG (PLASTIK SEGEL)", "g", 1000},
	"LEMBAR":             &uomConversion{"LEMBAR", "Pc(s)", 1},
	"ROLL":               &uomConversion{"ROLL", "Pc(s)", 1},
	"SET":                &uomConversion{"SET", "Set", 1},
	"PACK":               &uomConversion{"PACK", "g", 1},
}

type uomData struct {
	ID   int
	name string
}

var mfgUomByName = map[string]*uomData{}

func loadMfgUom(mfgServer, writeSince string) error {
	client, err := odoo.ClientConnect(mfgServer)
	if err != nil {
		return err
	}

	domain := odoo.List{
		"&",
		odoo.List{"active", "=", true},
	}

	if writeSince != "" {
		domain = append(domain, odoo.List{
			"|",
			odoo.List{"create_date", ">=", writeSince},
			odoo.List{"write_date", ">=", writeSince},
		})
	}

	records, err := client.SearchRead(
		"uom.uom",
		odoo.List{odoo.List{"active", "=", true}},
		[]string{"id", "name"},
	)
	if err != nil {
		return err
	}

	for _, rec := range records {
		id, err := odoo.IntField(rec, "id")
		if err != nil {
			continue
		}
		name, err := odoo.StringField(rec, "name")
		if err != nil {
			continue
		}
		mfgUomByName[name] = &uomData{id, name}
	}
	return nil
}

var categMapping = map[string]string{
	"Packaging Material": "Undefined PKG",
	"Raw Material":       "Undefined RM",
}

type categData struct {
	ID   int
	name string
}

var mfgCategByName = map[string]*categData{}

func loadMfgCateg(mfgServer string) error {
	client, err := odoo.ClientConnect(mfgServer)
	if err != nil {
		return err
	}

	records, err := client.SearchRead(
		"product.category",
		nil,
		[]string{"id", "name"},
	)
	if err != nil {
		return err
	}

	for _, rec := range records {
		id, err := odoo.IntField(rec, "id")
		if err != nil {
			continue
		}
		name, err := odoo.StringField(rec, "name")
		if err != nil {
			continue
		}
		mfgCategByName[name] = &categData{id, name}
	}
	return nil

}

func fetchNbmProducts(nbmServer string) <-chan *productInfo {
	nbmClient, err := odoo.ClientConnect(nbmServer)
	if err != nil {
		log.Errorf("Error connecting to %s: %v", nbmServer, err)
		return nil
	}

	log.Infof("Connected to NBM server at %s (%s)", nbmClient.GetBaseURL(), nbmClient.GetServerVersion())

	categIds, err := nbmClient.Search(
		"product.category",
		odoo.List{
			odoo.List{"name", "in", []string{
				"Packaging Material", "Raw Material",
			}},
		},
	)
	if err != nil {
		log.Error(err)
		return nil
	}

	log.Infof("Reading product data from NBM")
	nbmRecords, err := nbmClient.SearchRead(
		"product.template",
		odoo.List{
			odoo.List{"active", "=", true},
			odoo.List{"categ_id", "in", categIds},
			odoo.List{"create_date", ">=", "2019-10-01"},
		},
		[]string{"id", "name", "description", "categ_id", "default_code", "korporat_code", "uom_id"},
	)
	if err != nil {
		log.Error(err)
		return nil
	}

	out := make(chan *productInfo)

	go func() {
		for _, rec := range nbmRecords {
			prodID, _ := odoo.IntField(rec, "id")
			prodName, _ := odoo.StringField(rec, "name")
			prodDesc, _ := odoo.StringField(rec, "description")
			if prodDesc == "" {
				prodDesc = prodName
			}
			prodCode, _ := odoo.StringField(rec, "default_code")
			korporatCode, _ := odoo.StringField(rec, "korporat_code")
			if prodCode == "" && korporatCode == "" {
				log.Infof("Skipped (no code): %s", prodName)
				continue
			}
			if prodCode == "" {
				prodCode = korporatCode
			}
			categID, err := odoo.Many2oneField(rec, "categ_id")
			if err != nil {
				log.Warnf("Error reading category for product template %d - %s", prodID, prodCode)
			}
			uomID, err := odoo.Many2oneField(rec, "uom_id")
			if err != nil {
				log.Warnf("Error reading category for product template %d - %s", prodID, prodCode)
			}

			out <- &productInfo{prodID, prodName, prodDesc, prodCode, categID, uomID}

		}

		close(out)
	}()

	return out
}

func checkInMrp(mfgServer string, in <-chan *productInfo) <-chan *productInfo {
	client, err := odoo.ClientConnect(mfgServer)
	if err != nil {
		log.Error(err)
		return nil
	}

	out := make(chan *productInfo)

	go func() {
		for prod := range in {
			ids, err := client.Search(
				"product.template",
				odoo.List{
					"|",
					odoo.List{"default_code", "=", prod.code},
					odoo.List{"korporat_code", "=", prod.code},
				},
			)
			if err != nil {
				log.Warnf("Error while searching product %s: %v", prod.code, err)
				continue
			}
			if len(ids) == 0 {
				out <- prod
			}
		}
		close(out)
	}()

	return out
}

func buildNewProductData(mfgServer string, in ...<-chan *productInfo) <-chan map[string]interface{} {
	var wg sync.WaitGroup
	out := make(chan map[string]interface{})

	output := func(c <-chan *productInfo) {
		for prod := range c {
			catMap, ok := categMapping[prod.categID.Name]
			if !ok {
				log.Errorf("Unknown category %s", prod.categID.Name)
				continue
			}
			categ, ok := mfgCategByName[catMap]
			if !ok {
				log.Errorf("Cannot map category %s", catMap)
				continue
			}
			uomMap, ok := uomMapping[strings.ToUpper(prod.uomID.Name)]
			if !ok {
				log.Errorf("Unknown UOM %s", prod.uomID.Name)
				continue
			}
			uom, ok := mfgUomByName[uomMap.to]
			if !ok {
				log.Errorf("Cannot map UOM %s", uomMap.to)
				continue
			}
			out <- map[string]interface{}{
				"default_code":          prod.code,
				"korporat_code":         prod.code,
				"name":                  prod.description,
				"description_pickingin": prod.name,
				"categ_id":              categ.ID,
				"uom_id":                uom.ID,
				"uom_po_id":             uom.ID,
				"type":                  "product",
				"tracking":              "lot",
			}
		}
		wg.Done()
	}

	wg.Add(len(in))
	for _, c := range in {
		go output(c)
	}

	go func() {
		wg.Wait()
		close(out)
	}()

	return out
}

type createLog struct {
	code string
	id   int
}

func createProductInMfg(mfgServer string, in <-chan map[string]interface{}) <-chan *createLog {
	client, err := odoo.ClientConnect(mfgServer)
	if err != nil {
		log.Error(err)
		return nil
	}

	out := make(chan *createLog)
	go func() {
		for newVal := range in {
			id, err := client.Create("product.template", newVal)
			if err != nil {
				log.Error(err)
				id = -1
			}
			out <- &createLog{
				code: newVal["default_code"].(string),
				id:   id,
			}
		}
		close(out)
	}()
	return out
}

// ProductNbm2Mfg copies product.template from NBM to MFG
func ProductNbm2Mfg(nbmServer, mfgServer, writeSince string) error {
	log.Infof("Loading UOM from %s", mfgServer)
	if err := loadMfgUom(mfgServer, writeSince); err != nil {
		return err
	}

	log.Infof("Loading Category from %s", mfgServer)
	if err := loadMfgCateg(mfgServer); err != nil {
		return err
	}

	nbmProdChan := fetchNbmProducts(nbmServer)

	mrpCheckers := []<-chan *productInfo{
		checkInMrp(mfgServer, nbmProdChan),
		checkInMrp(mfgServer, nbmProdChan),
		checkInMrp(mfgServer, nbmProdChan),
		checkInMrp(mfgServer, nbmProdChan),
	}

	builtChan := buildNewProductData(mfgServer, mrpCheckers...)

	createChan := createProductInMfg(mfgServer, builtChan)

	for created := range createChan {
		log.Infof("Created %s --> %d", created.code, created.id)
	}

	return nil
}
